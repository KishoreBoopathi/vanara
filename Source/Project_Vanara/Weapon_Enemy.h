// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon_Base.h"
#include "Weapon_Enemy.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_VANARA_API AWeapon_Enemy : public AWeapon_Base
{
	GENERATED_BODY()

public:
	AWeapon_Enemy();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item | Collision")
		class UBoxComponent* CombatCollision;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	UFUNCTION()
	virtual void CombatCollisionOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void CombatCollisionOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
	void ActivateCollision();

	UFUNCTION(BlueprintCallable)
	void DeactivateCollision();

	void Detach(class AEnemy* Enemy);
};
