// Fill out your copyright notice in the Description page of Project Settings.


#include "Consumables.h"
#include "MainCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "Components/SphereComponent.h"
#include "MainPlayerController.h"

// Sets default values
AConsumables::AConsumables()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;

	CollisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionVolume"));
	CollisionVolume->SetupAttachment(GetRootComponent());

	IdleParticleComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("IdleParticleComponent"));
	IdleParticleComponent->SetupAttachment(GetRootComponent());

	bItemPicked = false;
}

// Called when the game starts or when spawned
void AConsumables::BeginPlay()
{
	Super::BeginPlay();

	CollisionVolume->OnComponentBeginOverlap.AddDynamic(this, &AConsumables::OnOverlapBegin);
	CollisionVolume->OnComponentEndOverlap.AddDynamic(this, &AConsumables::OnOverlapEnd);
	
}

// Called every frame
void AConsumables::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AConsumables::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Called..."));
	if (OtherActor)
	{
		AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor);
		if (MainCharacter)
		{
			FVector ActiveOverlappingItemLocation = this->GetActorLocation();
			MainCharacter->MainPlayerController->InteractingLocation = ActiveOverlappingItemLocation;
			MainCharacter->MainPlayerController->DisplayInteractMessage();
			MainCharacter->SetActiveOverlappingItem(this);
		}
	}

}
void AConsumables::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		AMainCharacter* MainCharacter = Cast<AMainCharacter>(OtherActor);
		if (MainCharacter)
		{
			MainCharacter->MainPlayerController->RemoveInteractMessage();
			MainCharacter->SetActiveOverlappingItem(nullptr);
		}
	}
}

void AConsumables::OnPickupBP(AMainCharacter* Picker)
{
	if (bIsItemFood)
		bItemPicked = Picker->IncreamentFood();
	else
		bItemPicked = Picker->IncreamentPotion();

	if (bItemPicked)
	{
		Picker->PickupLocations.Add(GetActorLocation());
		if (OverlapParticles)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), OverlapParticles, GetActorLocation(), FRotator(0.f), true);
		}
		if (OverlapSound)
		{
			UGameplayStatics::PlaySound2D(this, OverlapSound);
		}
		Destroy();
	}
}
