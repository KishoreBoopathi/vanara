// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LevelScriptActor.h"
#include "VanaraLevelScriptActor.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_VANARA_API AVanaraLevelScriptActor : public ALevelScriptActor
{
	GENERATED_BODY()
	
};
