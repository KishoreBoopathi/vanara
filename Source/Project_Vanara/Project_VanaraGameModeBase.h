// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Project_VanaraGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_VANARA_API AProject_VanaraGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
